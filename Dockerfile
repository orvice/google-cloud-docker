FROM gcr.io/google.com/cloudsdktool/cloud-sdk:alpine
RUN gcloud components install cloud-datastore-emulator

COPY entrypoint.sh /usr/bin/entrypoint.sh

ENTRYPOINT [ "/usr/bin/entrypoint.sh" ]